import {
	reducerDestinosViajes,
	DestinosViajesState,
	intializeDestinosViajesState,
	InitMyDataAction,
	NuevoDestinoAction,
	ElegidoFavoritoAction,
	VoteUpAction,
	VoteDownAction,
	ResetVotesAction
} from './destinos-viajes-state.model';
import {DestinoViaje} from './destino-viaje.model';

describe ('reducerDestinosViajes', () => {
	it('should reduce init data', () => {
		// setup (objetos que se van a necesitar para testear)
		const prevState: DestinosViajesState = intializeDestinosViajesState();
		const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
		// action ()
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
		// assertions (verificaciones)
		expect(newState.items.length).toEqual(2);
		expect(newState.items[0].nombre).toEqual('destino 1');
		// tear down (ejecutar el codigo necesario para borrar lo insertado en una tabla de base de datos)
	});

	it('should reduce new item added', () => {
		const prevState: DestinosViajesState = intializeDestinosViajesState();
		const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
		expect(newState.items.length).toEqual(1);
		expect(newState.items[0].nombre).toEqual('barcelona');
	});

	it('Esto deberia elegirlo como favorito ', () => {
       		// setup (objetos que se van a necesitar para testear)
       	const prevState: DestinosViajesState = intializeDestinosViajesState();
		const action: ElegidoFavoritoAction= new ElegidoFavoritoAction(new DestinoViaje('barcelona', 'url'));
		// action ()
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
		// assertions (verificaciones)
		expect(newState.favorito.setSelected(true)).toBe();
		// tear down (ejecutar el codigo necesario para borrar lo insertado en una tabla de base de datos)   
	});

	it('Esto deberia votar 1 arriba', () => {
		// setup (objetos que se van a necesitar para testear)
	    const prevState: DestinosViajesState = intializeDestinosViajesState();
		const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url',1,0));
		// const action: VoteDownAction = new VoteDownAction(['barcelona', 'url']);
		// action ()
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
		// assertions (verificaciones)
		expect(newState.items[0].votes).toEqual(1);
		// tear down (ejecutar el codigo necesario para borrar lo insertado en una tabla de base de datos)
	});

	it('Esto deberia votar 1 abajo', () => {
		// setup (objetos que se van a necesitar para testear)
	    const prevState: DestinosViajesState = intializeDestinosViajesState();
		const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url',0,1));
		// action ()
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
		// assertions (verificaciones)
		expect(newState.items[0].votesd).toEqual(1);
		// tear down (ejecutar el codigo necesario para borrar lo insertado en una tabla de base de datos)
	});

		it('Esto deberia borrar los votos', () => {
		// setup (objetos que se van a necesitar para testear)
	    const prevState: DestinosViajesState = intializeDestinosViajesState();
		const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url',0,0));
		// action ()
		const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
		// assertions (verificaciones)
		expect(newState.items[0].votes).toEqual(0);
		expect(newState.items[0].votesd).toEqual(0);

		// tear down (ejecutar el codigo necesario para borrar lo insertado en una tabla de base de datos)
	});

});
