import {v4 as uuid} from 'uuid';

export class DestinoViaje{

    private selected: boolean;
    public servicios: string[];
    id = uuid();

    constructor(public nombre:string, public imagenUrl:string, public votes: number = 0 , public votesd: number = 0 ){
        this.servicios= ['pileta', 'desayuno'];
    }
    isSelected(): boolean{
        //console.log(this.selected);
        return this.selected;
    }

    setSelected(s: boolean){
        this.selected = s;
    }

    voteUp() {
        this.votes++;
        return false
    }

    voteDown() {
        this.votesd++;
        return false
    }

    resetVotes() {
        this.votes = 0;
        this.votesd = 0;
        return false ;
    }
}